package main.java;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args){
        Integer suma = 0;
        List<Integer> frequencies = new ArrayList<>();
        Integer repeatedNumber = null;
        System.out.println("Searching for repeated frequency...\nIt may take a while...");
        while (repeatedNumber == null){
            Scanner scanner = new Scanner(Main.class.getResourceAsStream("/input.txt"));
            String currentLine;
            while (scanner.hasNextLine()) {
               currentLine = scanner.nextLine();
               Integer number = Integer.parseInt(currentLine.substring(1));
               if (currentLine.toCharArray()[0] == '+')
                    suma += number;
               else
                    suma -= number;
               if (frequencies.contains(suma)) {
                   repeatedNumber = suma;
                   break;
               }
               frequencies.add(suma);
            }
            scanner.close();
        }
        System.out.println("First repeated frequency: " + repeatedNumber);
    }


}
