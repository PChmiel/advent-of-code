package main.java;

import java.io.*;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {
        int total = 0;
        Scanner scanner = new Scanner(Main.class.getResourceAsStream("/input.txt"));
        String currentLine;
        System.out.println("Calculating total frequency...");
        while (scanner.hasNextLine()) {
           currentLine = scanner.nextLine();
           Integer number = Integer.parseInt(currentLine.substring(1));
           if (currentLine.toCharArray()[0] == '+')
               total += number;
           else
               total -= number;
        }
        scanner.close();
        System.out.println("Total frequency: " + total);
    }
}
