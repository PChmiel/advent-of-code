package main.java;

public class Claim {
    private int id;
    private Rectangle rectangle;

    public Claim(int id, Rectangle rectangle) {
        this.id = id;
        this.rectangle = rectangle;
    }

    public Rectangle getRectangle() {
        return rectangle;
    }

}
