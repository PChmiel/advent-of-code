package main.java;


import main.java.utils.inputConverter;

import java.util.*;

public class Main{
    public static void main(String[] args){
        Long startTime = System.currentTimeMillis();
        List<Claim> claims = scanForClaims();
        Fabric fabric = new Fabric();
        for (Claim c : claims){
            for (Claim otherClaim : claims){
                if (c.equals(otherClaim))
                    continue;
                Rectangle commonRectangle = Rectangle.getCommonRectangle(c.getRectangle(), otherClaim.getRectangle());
                fabric.updateAreaByCommonRectangle(commonRectangle);
            }
        }
        System.out.println("Total number of common inches: " + fabric.getTotalCommonInches());
        System.out.println("Executed in " + (System.currentTimeMillis() - startTime) + " miliseconds.");
    }

    private static List<Claim> scanForClaims(){
        List<Claim> claims = new ArrayList<>();
        Scanner scanner = new Scanner(Main.class.getResourceAsStream("/input.txt"));
        while(scanner.hasNextLine()){
            String currentLine = scanner.nextLine();
            claims.add(new Claim(inputConverter.getClaimId(currentLine), inputConverter.convertDataToRectangle(currentLine)));
        }
        scanner.close();
        return claims;
    }
}