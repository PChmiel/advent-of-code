package main.java;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Fabric {
    Map<Integer, Map<Integer, Boolean>> area;

    public Fabric() {
        this.area = new HashMap<>();
    }

    public void updateAreaByCommonRectangle(Rectangle rectangle){
        for(int i = rectangle.getStartX(); i < rectangle.getEndX(); i++) {
            Map<Integer, Boolean> insideMap = area.get(i) == null ? new HashMap<>() : area.get(i);
            for (int j = rectangle.getStartY(); j < rectangle.getEndY(); j++) {
                insideMap.put(j, true);
            }
            if (!insideMap.isEmpty()){
                area.put(i, insideMap);
            }
        }
    }
    public Integer getTotalCommonInches(){

        return getInsideMaps().stream()
                .map(Map::size)
                .reduce((a, b) -> a + b)
                .get();
    }
    public List<Map<Integer, Boolean>> getInsideMaps(){
        List<Map<Integer, Boolean>> insideMapsList = new ArrayList<>();
        area.values().stream().collect(Collectors.toCollection(() -> insideMapsList));
        return insideMapsList;
    }


}
