package main.java;


import main.java.utils.inputConverter;

import java.util.*;

public class Main{
    public static void main(String[] args){
        Long startTime = System.currentTimeMillis();
        Claim uniqueClaim = getUniqueClaim();
        if (uniqueClaim != null){
            System.out.println("Unique claim has id = " + uniqueClaim.getId());
        }
        else
            System.out.println("Unique claim has not been found!");
        System.out.println("Executed in " + (System.currentTimeMillis() - startTime) + " miliseconds.");
    }
    private static Claim getUniqueClaim(){
        List<Claim> claims = scanForClaims();
        for (Claim c : claims){
            int common = 0;
            for (Claim otherClaim : claims){
                if (c.equals(otherClaim))
                    continue;
                Rectangle commonRectangle = Rectangle.getCommonRectangle(c.getRectangle(), otherClaim.getRectangle());
                if (commonRectangle.getWidth() != 0 && commonRectangle.getHeight() != 0)
                    common++;
            }
            if(common==0)
                return c;
        }
        return null;

    }
    private static List<Claim> scanForClaims(){
        List<Claim> claims = new ArrayList<>();
        Scanner scanner = new Scanner(Main.class.getResourceAsStream("/input.txt"));
        while(scanner.hasNextLine()){
            String currentLine = scanner.nextLine();
            claims.add(new Claim(inputConverter.getClaimId(currentLine), inputConverter.convertDataToRectangle(currentLine)));
        }
        scanner.close();
        return claims;
    }
}