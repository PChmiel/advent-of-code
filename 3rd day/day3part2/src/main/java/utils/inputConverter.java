package main.java.utils;

import main.java.Rectangle;

public class inputConverter {

//Data's format: #1 @ 7,589 24x11
    public static Rectangle convertDataToRectangle(String data){
        return new Rectangle(getStartX(data),
                getStartY(data),
                getWidth(data),
                getHeight(data));

    }

    public static int getClaimId(String data){
        return Integer.parseInt(data.substring(1, data.indexOf(' ')));
    }

    private static int getStartX(String data){
        String xCoord = data.substring(data.indexOf('@') + 2,  data.indexOf(','));
        return Integer.parseInt(xCoord);
    }

    private static int getStartY(String data){
        String yCoord = data.substring(data.indexOf(',') + 1,  data.indexOf(' ', data.indexOf(',')));
        return Integer.parseInt(yCoord);
    }

    private static int getWidth(String data){
        String rectWidth = data.substring(data.indexOf(' ', data.indexOf(',')) + 1, data.indexOf('x'));
        return Integer.parseInt(rectWidth);
    }

    private static int getHeight(String data){
        String rectHeight = data.substring(data.indexOf('x') + 1);
        return Integer.parseInt(rectHeight);
    }

}
