package main.java;


public class Rectangle {
    private int startX;
    private int startY;

    private int width;
    private int height;


    public Rectangle(int startX, int startY, int width, int height) {
        this.startX = startX;
        this.startY = startY;
        this.width = width;
        this.height = height;
    }

    public static Rectangle getCommonRectangle(Rectangle first, Rectangle second){
        int newStartX = Math.min(first.getStartX(),second.getStartX()) + Math.abs(first.getStartX() - second.getStartX());
        int newStartY = Math.min(first.getStartY(),second.getStartY()) + Math.abs(first.getStartY() - second.getStartY());
        int newEndX = Math.max(first.getEndX(),second.getEndX()) - Math.abs(first.getEndX() - second.getEndX());
        int newEndY = Math.max(first.getEndY(),second.getEndY()) - Math.abs(first.getEndY() - second.getEndY());

        if (newStartX > newEndX || newStartY > newEndY){
            return new Rectangle(0,0,0,0);
        }
        return new Rectangle(newStartX, newStartY, newEndX - newStartX, newEndY - newStartY);
    }


    public int getEndX(){
        return startX + width;
    }

    public int getEndY(){
        return startY + height;
    }

    public int getStartX() {
        return startX;
    }

    public int getStartY() {
        return startY;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
