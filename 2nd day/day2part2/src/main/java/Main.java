package main.java;

import java.util.*;

public class Main {

    public static void main(String[] args){
        List<Line> lines = scanLines();
        final StringBuffer[] commonString = new StringBuffer[1];
        for(Line line : lines){
            List<Letter> currentLetterList = line.getLetterList();
            lines.stream().filter(otherLine -> !line.equals(otherLine)).forEach(otherLine ->{
                StringBuffer stringBuffer = new StringBuffer();
                List<Letter> otherLetterList = otherLine.getLetterList();
                for (int i = 0; i < currentLetterList.size(); i++){
                    if(currentLetterList.get(i).getLetter() == otherLetterList.get(i).getLetter())
                        stringBuffer.append(currentLetterList.get(i).getLetter());
                }
                if (stringBuffer.length() + 1 == currentLetterList.size()) {
                    commonString[0] = stringBuffer;
                }

            });
        }
        System.out.println("Common string: " + commonString[0]);
    }

    private static List<Line> scanLines(){
        List<Line> lines = new ArrayList<>();
        Scanner scanner = new Scanner(Main.class.getResourceAsStream("/input.txt"));
        while (scanner.hasNextLine())
            lines.add(new Line(scanner.nextLine(), true));
        scanner.close();
        return lines;
    }
}
