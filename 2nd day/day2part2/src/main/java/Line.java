package main.java;

import java.util.*;

public class Line {
    private List<Letter> letterList;
    private String letterString;


    public Line(String lineString, Boolean notFixed){
        List<Letter> letters = new ArrayList<>();
        char[] charLetters = lineString.toCharArray();
        for (char letter : charLetters){
            letters.add(new Letter(letter));
        }
        this.letterList = letters;
        this.letterString = lineString;
    }


    public List<Letter> getLetterList() {
        return letterList;
    }

}
