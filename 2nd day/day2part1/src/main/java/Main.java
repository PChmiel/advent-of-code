package main.java;


import java.util.*;

public class Main {

    public static void main(String[] args){
        int twices = 0;
        int triples= 0;
        Scanner scanner = new Scanner(Main.class.getResourceAsStream("/input.txt"));
        while (scanner.hasNextLine()) {
            Line currentLine = new Line(scanner.nextLine());
            if (currentLine.containsTwice())
                twices++;
            if (currentLine.containsTriples())
                triples++;
        }
        scanner.close();

        System.out.println("Occured two times: " + twices + "\nOccured three times: " + triples + ".");
        System.out.println("Checksum: " + twices + " * " + triples + " = " + twices*triples);
    }
}
