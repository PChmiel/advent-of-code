package main.java;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Line {
    private List<Letter> letterList;


    public Line(String lineString){
        List<Letter> letters = new ArrayList<>();
        Map<Character, Integer> letterMap = getLetterMap(lineString);
        for(Character letter : letterMap.keySet()){
            letters.add(new Letter(letter, letterMap.get(letter)));
        }
        this.letterList = letters;
    }

    private static Map<Character, Integer> getLetterMap(String line){
        Map<Character, Integer> letterMap = new HashMap<>();
        char[] letters = line.toCharArray();
        for (char letter : letters){
            if(!letterMap.containsKey(letter))
                letterMap.put(letter, 1);
            else{
                letterMap.put(letter, letterMap.get(letter) + 1);
            }
        }
        return letterMap;
    }
    public Boolean containsTwice(){
        return letterList.stream().anyMatch(Letter::getOccurredTwoTimes);
    }

    public Boolean containsTriples(){
        return letterList.stream().anyMatch(Letter::getOccurredThreeTimes);
    }
}
