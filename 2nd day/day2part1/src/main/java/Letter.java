package main.java;

public class Letter {

    private char letter;
    private Boolean occurredTwoTimes = false;
    private Boolean occurredThreeTimes = false;

    public Letter(char letter, int times) {
        this.letter = letter;
        if(times == 2)
            occurredTwoTimes = true;
        else if(times == 3)
            occurredThreeTimes = true;
    }

    public Boolean getOccurredTwoTimes() {
        return occurredTwoTimes;
    }

    public Boolean getOccurredThreeTimes() {
        return occurredThreeTimes;
    }
}
